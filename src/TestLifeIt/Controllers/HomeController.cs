﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TestLifeIt.DataBase.SQLite;
using TestLifeIt.DataBase.SQLite.Models;
using TestLifeIt.Models;

namespace TestLifeIt.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        public IActionResult Index()
        {
            using (var context = new SqLiteContext())
            {
                var result = from h in context.Historys select h;
                ViewBag.History = result.ToList();
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Search(SearchModel model)
        {
            var client = new HttpClient();

            var values = new Dictionary<string, string>
            {
                { "media", "music" },
                { "limit", "50" },
                { "term", model.SearchString }
            };

            var content = new FormUrlEncodedContent(values);

            var response = client.PostAsync("https://itunes.apple.com/search", content);
            var responseString = response.Result.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<SearchResult>(responseString.Result);

            using (var context = new SqLiteContext())
            {
                foreach (var row in context.Historys.Take(50))
                {
                    context.Historys.Remove(row);
                }

                context.SaveChanges();
            }

            using (var context = new SqLiteContext())
            {
                foreach (var value in result.Results)
                {
                    var history = new History
                    {
                        ArtistName = value.artistName,
                        ArtworkUrl60 = value.artworkUrl60
                    };

                    context.Historys.Add(history);
                }
             
                context.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}