﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestLifeIt.Models
{
    public class SearchResult
    {
        [JsonProperty("resultCount")]
        public int ResultCount;

        [JsonProperty("results")]
        public List<Result> Results;
    }

    public class Result
    {
         public string wrapperType;
         public string kind;
         public long artistId;
         public long collectionId;
         public long trackId;
         public string artistName;
         public string collectionName;
         public string trackName;
         public string collectionCensoredName;
         public string trackCensoredName;
         public string artistViewUrl;
         public string collectionViewUrl;
         public string trackViewUrl;
         public string previewUrl;
         public string artworkUrl30;
         public string artworkUrl60;
         public string artworkUrl100;
         public string collectionPrice;
         public decimal trackPrice;
         public string releaseDate;
         public string collectionExplicitness;
         public string trackExplicitness;
         public int discCount;
         public int discNumber;
         public int trackCount;
         public int trackNumber;
         public long trackTimeMillis;
         public string country;
         public string currency;
         public string primaryGenreName;
         public bool isStreamable;
    }
}