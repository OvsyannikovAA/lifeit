﻿using System.ComponentModel.DataAnnotations;

namespace TestLifeIt.DataBase.SQLite.Models
{
    public class History
    {
        [Key]
        public int Id { get; set; }

        public string ArtistName { get; set; }
        public string ArtworkUrl60 { get; set; }
    }
}