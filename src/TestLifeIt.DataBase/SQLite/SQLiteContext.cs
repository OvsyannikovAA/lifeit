﻿using Microsoft.EntityFrameworkCore;
using TestLifeIt.DataBase.SQLite.Models;

namespace TestLifeIt.DataBase.SQLite
{
    public sealed class SqLiteContext : DbContext
    {
        public DbSet<History> Historys { get; set; }

        public SqLiteContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=TestLifeItStorage.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<History>().ToTable("Historys");
        }
    }
}